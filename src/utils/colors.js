import { multiply } from "color-blend";

/**
 * Convert a decimal value to hex
 *
 * @param {number} d - Decimal value to convert
 * @returns {string} hexadecimal string representation
 */
const d2h = (d) => {
  return d.toString(16).padStart(2, "0");
};

/**
 * Convert a hex value to decimal
 *
 * @param {string} h - Hexadecimal string value to convert
 * @returns {string} decimal representation
 */
const h2d = (h) => {
  return parseInt(h, 16);
};

/**
 * Convert a decimal value to hex
 *
 * @param {string} hex - Hexadecimal color to convert
 * @returns {RGBA} RGBA object
 */
const hexToRgba = (hex) => {
  hex = hex.replace(" ", "");
  if (hex.startsWith("rgb(")) {
    hex = hex.replace("rgb(", "").replace(")", "");
    const values = hex.split(",");
    return {
      r: parseInt(values[0]),
      g: parseInt(values[1]),
      b: parseInt(values[2]),
      a: 1,
    };
  }
  if (hex.startsWith("rgba(")) {
    hex = hex.replace("rgba(", "").replace(")", "");
    const values = hex.split(",");
    return {
      r: parseInt(values[0]),
      g: parseInt(values[1]),
      b: parseInt(values[2]),
      a: parseInt(values[3]),
    };
  }
  hex = hex.replace(/#/g, "");
  if (hex.length === 6) {
    return {
      r: h2d(hex.slice(0, 2)),
      g: h2d(hex.slice(2, 4)),
      b: h2d(hex.slice(4)),
      a: 1,
    };
  }
  if (hex.length === 8) {
    return {
      r: h2d(hex.slice(0, 2)),
      g: h2d(hex.slice(2, 4)),
      b: h2d(hex.slice(4, 6)),
      a: h2d(hex.slice(6)),
    };
  }
  throw new Error("ERROR converting color string to RGBA interface");
};

/**
 * Convert from RGBA to hexadecimal string
 *
 * @param {RGBA} color - RGBA color to convert
 * @returns {string} channels mixin result
 */
const rgbaToHex = (color) => `#${d2h(color.r)}${d2h(color.g)}${d2h(color.b)}`;

/**
 * Mix 2 channels
 *
 * @param {number} channelA - First channel
 * @param {number} channelB - Second channel
 * @param {number} weight - Weight to apply on mixin
 * @returns {number} channels mixin result
 */
const mixChannel = (channelA, channelB, weight) =>
  Math.round(channelB + (channelA - channelB) * (weight / 100.0));

/**
 * Mix 2 colors
 *
 * @param {RGBA} color - Color to mix
 * @param {RGBA} base - Color to mix with
 * @param {number} weight - Weight to apply to base color while mixing
 * @returns {RGBA} new RGBA color after mixin
 */
const mixColor = (color, base, weight) => ({
  r: mixChannel(base.r, color.r, weight),
  g: mixChannel(base.g, color.g, weight),
  b: mixChannel(base.b, color.b, weight),
  a: 1,
});

/**
 * Darks a color with a defined weight
 *
 * @param {string} color - Color to mix, in hexadecimal
 * @param {string} base - Color to mix with, in hexadecimal
 * @param {number} weight - Weight to apply to base color while mixing
 * @returns {string} new hexadecimal color after mixin
 */
const darken = (color, base = "#000000", weight = 50) => {
  const baseRgba = hexToRgba(base);
  const colorRgba = hexToRgba(color);
  baseRgba.a = weight / 100.0;
  return rgbaToHex(multiply(baseRgba, colorRgba));
};

/**
 * Lights a color with a defined weight
 *
 * @param {string} color - Color to mix in hexadecimal
 * @param {string} base - Color to mix with in hexadecimal
 * @param {number} weight - Weight to apply to base color while mixing
 * @returns {string} new hexadecimal color after mixin
 */
const lighten = (color, base = "#FFFFFF", weight = 50) => {
  const baseRgba = hexToRgba(base);
  const colorRgba = hexToRgba(color);
  return rgbaToHex(mixColor(colorRgba, baseRgba, weight));
};

/**
 * Checks if color is a valid CSS color
 *
 * @param {string=} color - The css color to validate
 * @returns {boolean} true if it is a valid css color
 */
const isCssColor = (color) =>
  !!color && !!color.match(/^(#|var\(--|(rgb|hsl)a?\()/);

export { isCssColor, lighten, darken };
