import { isProduction } from "./is_environment";

const GA_TRACKING_ID = isProduction
  ? process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS ?? ""
  : "";
console.log({ GA_TRACKING_ID });
type GTagEvent = {
  action: string;
  category?: string;
  label?: string;
  value?: number;
};

const gPageView = (url: URL): void => {
  window.gtag("config", GA_TRACKING_ID, {
    page_path: url,
  });
};

const gEvent = ({ action, category, label, value }: GTagEvent) => {
  window.gtag("event", action, {
    event_category: category,
    event_label: label,
    value,
  });
};

export { gEvent, gPageView, GA_TRACKING_ID };
