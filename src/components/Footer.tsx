import styled from "styled-components";
import ButtonLinkText from "components/Primitives/ButtonLinkText";
import SocialNetworks from "./SocialNetworks";
import H3 from "./Primitives/H3";
import personalInfo from "data/personalInfo";

const FooterContainer = styled.footer`
  width: 100%;
`;

const FooterCopyright = styled.div`
  padding: 2rem;
  display: flex;
  gap: 1rem;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.colors.brand.primary.darken0};
  text-decoration: underline;
  .split-line {
    width: 3px;
    height: 1.5rem;
    background-color: white;
  }
`;

const FooterContent = styled.div`
  padding: 1.5rem;
  background: linear-gradient(
      0deg,
      rgba(255, 255, 255, 0.25),
      rgba(255, 255, 255, 0.25)
    ),
    #150be8;
  h3,
  p {
    padding: 0.5rem;
  }
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    padding: 5rem 5rem 0rem;
  }
`;

const Footer = () => {
  return (
    <FooterContainer>
      <FooterContent>
        <H3>{personalInfo.name}</H3>
        <p>{personalInfo.footerNote}</p>
        <SocialNetworks />
      </FooterContent>
      <FooterCopyright>
        <a href="https://luisperez.co" target="_blank" rel="noreferrer">
          <ButtonLinkText link>Designed by: Luis Pérez</ButtonLinkText>
        </a>
        <div className="split-line"></div>
        <ButtonLinkText link>Developed by: José Truyol</ButtonLinkText>
      </FooterCopyright>
    </FooterContainer>
  );
};

export default Footer;
