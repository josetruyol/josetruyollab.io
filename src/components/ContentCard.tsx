import styled from "styled-components";

const ContentCardContainer = styled.article`
    position: absolute;
    right: -110px;
    top: -180px;
    padding: 120px 190px 120px 110px;
    width: 65%;
    min-height: 600px;
    h2 {
      font-size: 60px;
      margin: 0;
      font-weight: 700;
    }
  
`;

const ContentCard = () => {
  return (
      <ContentCardContainer>
        <h2>Who am I?</h2>
        <p>
          Sed odio risus libero, proin nulla sed sit. Enim nibh platea sit sed
          condimentum. Justo, enim viverra egestas ut tincidunt aliquam
          faucibus. At sollicitudin fermentum cursus tellus leo. Lobortis libero
          aliquam dolor, id purus. Lectus euismod quis platea commodo, id eget.
        </p>
      </ContentCardContainer>
  );
};

export default ContentCard;
