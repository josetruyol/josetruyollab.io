import styled from "styled-components";
import H5 from "./Primitives/H5";
import { Briefcase, Calendar, MapPin, FileText } from "react-feather";

const WorkCardContainer = styled.article`
  border-radius: 1rem;
  background-color: ${(props) => props.theme.colors.layout.darken0};
  padding: 1rem;
  width: clamp(16.875rem, calc(100vw - 4rem), 40rem);
  display: flex;
  flex-direction: column;
  gap: 8px;
  .split-line {
    width: 100%;
    height: 1px;
    background-color: #f2f2f2;
  }
  .work-card__item {
    display: flex;
    align-items: center;
    gap: 0.5rem;
    padding: 0.5rem;
    .duration-container {
      &:first-child {
        margin-bottom: 0.5rem;
      }
    }
  }
  p {
    padding: 1rem;
  }
  span {
    display: block;
  }
`;

interface WorkCardProps {
  title: string;
  company: string;
  from: string;
  to: string;
  duration: string;
  location: string;
  description: string;
}

const WorkCard = ({
  title,
  company,
  from,
  to,
  duration,
  location,
  description,
}: WorkCardProps) => {
  return (
    <WorkCardContainer>
      <H5 bold>{title}</H5>
      <div className="split-line"></div>
      <div className="work-card__item">
        <Briefcase />
        <span className="bold">{company}</span>
      </div>
      <div className="work-card__item">
        <Calendar />
        <div className="duration-container">
          <span className="bold">
            {from} - {to}
          </span>
          <span>{duration}</span>
        </div>
      </div>
      <div className="work-card__item">
        <MapPin />
        <span className="bold">{location}</span>
      </div>
      <div className="work-card__item">
        <FileText />
        <span className="bold">Description</span>
      </div>
      {description.split("\n").map((x, idx) => (
        <p key={idx}>{x}</p>
      ))}
    </WorkCardContainer>
  );
};

export default WorkCard;
