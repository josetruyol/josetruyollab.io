import React from "react";
import { Award } from "react-feather";
import styled from "styled-components";
import H6 from "./Primitives/H6";
import SubtitleText from "./Primitives/SubtitleText";
import { format as dateFormat } from "date-fns";

const CertificationCardContainer = styled.article`
  position: relative;
  overflow: visible;
  padding-top: 5rem;
  width: clamp(16.875rem, calc(100vw - 4rem), 20.25rem);
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    width: 22.5rem;
  }
  .icon-container {
    background-color: white;
    box-shadow: ${(props) => props.theme.shadows.dp05};
    position: absolute;
    transform: translateY(-50%);
    left: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 1rem;
    padding: 3.5rem;
    color: ${(props) => props.theme.colors.brand.primary.darken0};
  }
  .certification-card__content {
    background-color: ${(props) => props.theme.colors.layout.darken0};
    border-radius: 1rem;
    padding: 80px 2rem 2rem 2rem;
    & > * {
      display: block;
    }
    .split-line {
      width: 100%;
      height: 2px;
      border-radius: 2px;
      margin: 0.5rem 0;
      background-color: ${(props) => props.theme.colors.brand.secondary.base};
    }
  }
`;
interface CertificationCardProps {
  issuer: string;
  title: string;
  from: string;
  to?: string;
}
const CertificationCard = ({
  issuer,
  title,
  from,
  to,
}: CertificationCardProps) => {
  return (
    <CertificationCardContainer>
      <div className="icon-container">
        <Award size={40}></Award>
      </div>
      <div className="certification-card__content">
        <H6 bold>{title}</H6>
        <div className="split-line"></div>
        <SubtitleText bold>{issuer}</SubtitleText>
        <p>
          <SubtitleText>
            {from.toLocaleString()}
            {to ? `- ${to}` : ""}
          </SubtitleText>
        </p>
      </div>
    </CertificationCardContainer>
  );
};

export default CertificationCard;
