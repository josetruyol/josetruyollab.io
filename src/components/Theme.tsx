import { ReactElement, ReactPropTypes } from "react";
import { DefaultTheme, ThemeProvider } from "styled-components";
import { lighten } from "../utils/colors";

const colors = {
  brand: {
    primary: {
      base: "#150BE8",
      darken0: "#1109BA",
      darken1: "#0D078B",
      darken2: "#08045D",
      lighten0: lighten("#08045D", "#FFFFFF", 25),
    },
    secondary: {
      base: "#30E8BD",
      darken0: "#26BA97",
      darken1: "#1D8B71",
      darken2: "#135D4C",
    },
  },
  layout: {
    darken0: "#01093C",
    darken1: lighten("#01093C", "#FFFFFF", 20),
    darken2: lighten("#01093C", "#FFFFFF", 40),
    darken3: lighten("#01093C", "#FFFFFF", 60),
    darken4: lighten("#01093C", "#FFFFFF", 80),
    lighten0: "#C0C5E5",
    lighten1: lighten("#C0C5E5", "#FFFFFF", 20),
    lighten2: lighten("#C0C5E5", "#FFFFFF", 40),
    lighten3: lighten("#C0C5E5", "#FFFFFF", 60),
    lighten4: lighten("#C0C5E5", "#FFFFFF", 80),
    overlay1: "rgba(21, 11, 232, 0.04)",
    overlay2: "rgba(21, 11, 232, 0.12)",
    overlay3: "rgba(21, 11, 232, 0.16)",
  },
};
const shadows = {
  dp01: "0px 0px 1px rgba(40, 41, 61, 0.08), 0px 0.5px 2px rgba(96, 97, 112, 0.16)",
  dp02: "0px 0px 1px rgba(40, 41, 61, 0.04), 0px 2px 4px rgba(96, 97, 112, 0.16)",
  dp03: "0px 0px 2px rgba(40, 41, 61, 0.04), 0px 4px 8px rgba(96, 97, 112, 0.16)",
  dp04: "0px 2px 4px rgba(40, 41, 61, 0.04), 0px 8px 16px rgba(96, 97, 112, 0.16)",
  dp05: "0px 2px 8px rgba(40, 41, 61, 0.04), 0px 16px 24px rgba(96, 97, 112, 0.16)",
  dp06: "0px 2px 8px rgba(40, 41, 61, 0.08), 0px 20px 32px rgba(96, 97, 112, 0.24);",
};
const breakpoints = {
  sm: "768px",
};
const themeExtra = {
  backgroundColor: colors.layout.lighten3,
  menu: {
    activeColor: colors.brand.secondary.base,
    mobile: {
      background: "#3A406A",
    },
    desktop: {
      textColor: "#08045D",
      activeTextColor: colors.brand.primary.base,
    },
  },
};

const theme: DefaultTheme = { breakpoints, colors, shadows, ...themeExtra };

type ThemeProps = {
  children: ReactElement;
};
const Theme = ({ children }: ThemeProps) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
export default Theme;
