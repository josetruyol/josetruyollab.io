import { useRouter } from "next/router";
import Link, { LinkProps } from "next/link";
import React from "react"
interface NavLinkProps extends React.PropsWithChildren<LinkProps> {
  activeClassName?: string
}
const NavLink = ({ href, children, activeClassName, ...props }: NavLinkProps) => {
  const { pathname } = useRouter();
  return <Link href={href} >
    <a className={pathname === href ? activeClassName : '' } {...props}>
      {children}
    </a>
  </Link>
}

export default NavLink;