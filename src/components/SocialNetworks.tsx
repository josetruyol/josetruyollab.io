import styled from "styled-components";
import CircularIcon from "./CircularIcon";
import {
  Linkedin,
  Twitter,
  Instagram,
  Gitlab,
  Mail,
  GitHub,
} from "react-feather";
import personalInfo from "../data/personalInfo";
import { gEvent } from "utils/ga";

const SocialNetworksContainer = styled.div<{ smColum?: boolean }>`
  display: flex;
  flex-wrap: wrap;
  padding: 1rem;
  gap: clamp(0.75rem, 1.5rem, 2rem);
  a {
    display: flex;
    text-decoration: none;
  }
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    flex-direction: ${(props) => (props.smColum ? "column" : "row")};
  }
`;
interface SocialNetworksProps {
  variant?: "secondary" | "primary";
  smColum?: boolean;
}
const SocialNetworks = ({ variant, smColum }: SocialNetworksProps) => {
  const visitEvent = (name: string) =>
    gEvent({
      action: "social_network_visit",
      category: name,
      label: name,
    });
  return (
    <SocialNetworksContainer className={variant} smColum={smColum}>
      <a
        href={personalInfo.socialNetworks.linkedInd}
        target="_blank"
        rel="noreferrer"
        onClick={() => visitEvent("linkedin")}
      >
        <CircularIcon variant={variant}>
          <Linkedin size={20} />
        </CircularIcon>
      </a>
      <a
        href={personalInfo.socialNetworks.twitter}
        target="_blank"
        rel="noreferrer"
        onClick={() => visitEvent("twitter")}
      >
        <CircularIcon variant={variant}>
          <Twitter size={20} />
        </CircularIcon>
      </a>
      <a
        href={personalInfo.socialNetworks.instagram}
        target="_blank"
        rel="noreferrer"
        onClick={() => visitEvent("instagram")}
      >
        <CircularIcon variant={variant}>
          <Instagram size={20} />
        </CircularIcon>
      </a>
      <a
        href={personalInfo.socialNetworks.gitLab}
        target="_blank"
        rel="noreferrer"
        onClick={() => visitEvent("gitLab")}
      >
        <CircularIcon variant={variant}>
          <Gitlab size={20} />
        </CircularIcon>
      </a>
      <a
        href={personalInfo.socialNetworks.gitHub}
        target="_blank"
        rel="noreferrer"
        onClick={() => visitEvent("gitHub")}
      >
        <CircularIcon variant={variant}>
          <GitHub size={20} />
        </CircularIcon>
      </a>
      <a
        href={`mailto:${personalInfo.email}`}
        onClick={() => visitEvent("email")}
      >
        <CircularIcon variant={variant}>
          <Mail size={20} />
        </CircularIcon>
      </a>
    </SocialNetworksContainer>
  );
};

export default SocialNetworks;
