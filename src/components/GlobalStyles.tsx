import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
/* Resets */
html {
  box-sizing: border-box;
}

html *, html *::after, html *::before {
  box-sizing: inherit;
}
html,
body {
  height: 100%;
}

body, h1, h2, h3, h4, h5, h6, p, figure, picture, .subtitle-text {
  margin: 0;
  color: white;
}

body {
  line-height: 1.5;
  min-height: 100vh;
  font-family: "Montserrat", sans-serif;
  font-size: 16px;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  background-color: ${(props) => props.theme.backgroundColor};
  position: relative;
  overflow-x: hidden;
}

/* #__next {
  max-width: 90rem;
  margin: 0 auto;
} */

img, picture {
  max-width: 100%;
  display: block;
}

input, button, textarea, select {
  font: inherit;
}

@media (prefers-reduced-motion: reduce){
  *, *::before, *::after {
    animation-duration:  0.01ms !important;
    animation-iteration-count:  1 !important;
    transition-duration:  0.01ms !important;
    scroll-behavior:  auto !important;
  }
}

main {
  margin-top: 4rem !important;
}
section {
  width: 100%;
  position: relative;
}
.bold {
  font-weight: bold;
}
`;

export default GlobalStyles;
