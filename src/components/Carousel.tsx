import styled from "styled-components";
import Slider from "react-slick";

// react-responsive-carousel
const Carousel = styled(Slider).attrs({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  variableWidth: true,
})`
  padding: 3rem 0;
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    padding: 0 2rem 3rem 2rem;
  }
  .slick-dots {
    left: 0;
    bottom: 0;
    button {
      &::before {
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        content: "";
        width: 0.25rem;
        height: 0.25rem;
        border: 2px solid white;
        border-radius: 100%;
        opacity: 1;
        transition: all 0.2s ease;
      }
    }
    .slick-active {
      button {
        &::before {
          background-color: ${(props) =>
            props.theme.colors.brand.secondary.base};
          opacity: 1;
          width: 0.75rem;
          height: 0.75rem;
          border: 2px solid
            ${(props) => props.theme.colors.brand.secondary.base};
        }
      }
    }
  }
  .slick-track {
    display: flex;
    gap: 2rem;
    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      gap: 2.5rem;
    }
  }
  .slick-arrow {
    display: none !important;
    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      display: block !important;
      &.slick-prev {
        left: 0;
      }
      &.slick-next {
        right: 0;
      }
    }
  }
`;

export default Carousel;
