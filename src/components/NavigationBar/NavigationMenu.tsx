import React, { ForwardedRef, forwardRef } from "react";
import styled from "styled-components";

const NavigationMenuContainer = styled.nav`
  min-height: calc(100vh - 4rem);
  position: absolute;
  z-index: 10;
  top: 4rem;
  left: 0px;
  width: 100%;
  background-color: ${(props) => props.theme.menu.mobile.background};
  color: white;
  padding: 24px;
  overflow: hidden;
  transition: transform 0.5s ease;
  height: 100%;
  transform: translateX(100%);
  &[data-active="true"] {
    transform: translateX(0px);
  }
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    min-height: auto;
    position: static;
    background-color: transparent;
    transform: translateX(0px);
    width: auto;
    padding: 2px;
    color: ${(props) => props.theme.menu.desktop.textColor};
  }
`;
const NavigationMenuList = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  padding: 0;
  margin: 0;
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    flex-direction: row;
  }
`;

interface NavigationMenuProps {
  open: boolean;
}

const NavigationMenu = forwardRef(
  (
    { open, children }: React.PropsWithChildren<NavigationMenuProps>,
    ref: ForwardedRef<HTMLElement>
  ) => {
    return (
      <NavigationMenuContainer data-active={open} ref={ref}>
        <NavigationMenuList>{children}</NavigationMenuList>
      </NavigationMenuContainer>
    );
  }
);
NavigationMenu.displayName = "NavigationMenu";
export default NavigationMenu;
