import NavLink from "../NavLink";
import { LinkProps } from "next/link";
import React, { memo } from "react";
import styled from "styled-components";

const NavigationItemContainer = styled.li`
  display: block;
  font-weight: 600;
  position: relative;
  &:hover {
    a::after {
      width: 100%;
    }
  }
  a {
    display: block;
    text-decoration: none;
    color: inherit;
    padding: 0.5rem 1rem;
    width: 100%;
      @media only screen and (min-width: ${(props) =>
          props.theme.breakpoints.sm}) {
    padding: 0.75rem 1rem;
      }
    &::after {
      position: absolute;
      bottom: 0;
      left: 0;
      content: "";
      width: 0;
      height: 2px;
      background-color: ${(props) => props.theme.colors.brand.secondary.base};
      transition: width 0.5s ease-in-out;
      border-radius: 4px;
    }
    &.active,
    &:active {
      color: inherit;
      @media only screen and (min-width: ${(props) =>
          props.theme.breakpoints.sm}) {
        color: ${(props) => props.theme.menu.desktop.activeTextColor};
      }
      &::after {
        width: 100%;
      }
    }
  }
`;
interface NavigationItemProps extends React.PropsWithChildren<LinkProps> {
  external?: boolean;
}
const NavigationItem = ({ children, href, external, ...props }: NavigationItemProps) => {
  return (
    <NavigationItemContainer >
      {external ? (
        <a href={href.toString()}>{children}</a>
      ) : (
        <NavLink href={href} {...props}>{children}</NavLink>
      )}
      <div className="border" />
    </NavigationItemContainer>
  );
};
export default memo(NavigationItem);
