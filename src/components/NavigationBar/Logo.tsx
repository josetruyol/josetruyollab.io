
import styled from "styled-components";
import NavLink from "../NavLink";

const LogoContainer = styled(NavLink)`
  display: inline-flex;
  align-items: center;
  font-family: "Noto Sans", sans-serif;
  text-decoration: none;
  color: inherit;
  margin: 0;
  div {

      display: flex;
      flex-direction: column;
      align-items: flex-start;
      span {
        text-transform: uppercase;
        font-size: 1rem;
        font-weight: 700;
        color: ${(props) => props.theme.colors.layout.darken0};
      
    }
  }
`;

const Logo = () => {
  return (
    <LogoContainer href="/">
      {/* <img src="logo192.png" alt="" width="62" /> */}
      <div>
        <span>Jose</span>
        <span>Truyol</span>
      </div>
    </LogoContainer>
  );
};

export default Logo;
