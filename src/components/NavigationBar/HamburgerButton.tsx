import React, { ForwardedRef, forwardRef } from "react";
import styled from "styled-components";

const HamburgerButtonContainer = styled.button`
  padding: 8px;
  height: 48px;
  width: 48px;
  border: none;
  background-color: transparent;
  cursor: pointer;
  text-align: left;
  .wrapper {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    position: relative;
    .line {
      width: 100%;
      height: 3px;
      border-radius: 3px;
      background-color: ${(props) => props.theme.colors.brand.primary.base};
      transition: all 0.2s ease-in-out;
      &::before,
      &::after {
        content: "";
        position: absolute;
        width: 100%;
        height: 3px;
        background-color: ${(props) => props.theme.colors.brand.primary.base};
        border-radius: 3px;
        transition: all 0.2s ease-in-out;
      }
      &::before {
        transform: translateY(-9px);
      }
      &::after {
        transform: translateY(9px);
      }
    }
  }
  &[data-active="true"] {
    background-color: ${(props) => props.theme.colors.layout.overlay1};
    .wrapper {
      .line {
        background-color: transparent;
        &::before,
        &::after {
          background-color: ${(props) =>
            props.theme.colors.brand.secondary.base};
        }
        &::before {
          transform: translateX(-50%) rotate(45deg) translate(11.5px, -11.5px);
        }
        &::after {
          transform: translateX(-50%) rotate(-45deg) translate(11.5px, 11.5px);
        }
      }
    }
  }
  &:hover {
    background-color: ${(props) => props.theme.colors.layout.overlay1};
  }
  &:active {
    background-color: ${(props) => props.theme.colors.layout.overlay3};
  }
  @media only screen and (min-width: 768px) {
    display: none;
  }
`;

interface HamburgerButtonProps {
  active?: boolean;
}
const HamburgerButton = forwardRef(
  (
    { active, ...props }: React.PropsWithChildren<HamburgerButtonProps>,
    ref: ForwardedRef<HTMLButtonElement>
  ) => {
    return (
      <HamburgerButtonContainer data-active={active} ref={ref} {...props}>
        <div className="wrapper">
          <div className="line"></div>
        </div>
      </HamburgerButtonContainer>
    );
  }
);
HamburgerButton.displayName = "HamburgerButton";
export default HamburgerButton;
