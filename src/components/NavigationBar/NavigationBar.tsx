import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import HamburgerButton from "./HamburgerButton";
import Logo from "./Logo";
import NavigationItem from "./NavigationItem";
import NavigationMenu from "./NavigationMenu";
import useOuterClick from "../../hooks/OuterClick";
import personalInfo from "data/personalInfo";

const NavigationBarContainer = styled.header`
  width: 100%;
  position: fixed;
  top: 0;
  z-index: 5;
  background-color: transparent;
  transition: all 0.2s ease-in-out;
  .navigation-bar__wrapper {
    max-width: 90rem;
    margin: 0 auto;
    padding: 0.5rem 1.5rem;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      flex-wrap: nowrap;
      padding: 1.5rem 5rem;
    }
  }
  &.active {
    background-color: white;
    box-shadow: ${(props) => props.theme.shadows.dp04};
  }
`;
interface NavigationBar {}
const NavigationBar = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [activeClass, setIsOnTop] = useState("");
  useEffect(() => {
    document.addEventListener("scroll", () => {
      setIsOnTop(window.scrollY > 50 ? "active" : "");
    });
  });

  const innerRef = useRef("nav");
  const burgerRef = useRef("burger");
  useOuterClick(innerRef, (e: Event) => {
    // @ts-ignore
    if (e.target !== burgerRef.current) menuOpen && setMenuOpen(false);
  });
  return (
    <NavigationBarContainer className={activeClass}>
      <div className="navigation-bar__wrapper">
        <Logo></Logo>
        <HamburgerButton
          active={menuOpen}
          // @ts-ignore
          ref={burgerRef}
          onClick={() => setMenuOpen(!menuOpen)}
        />

        <NavigationMenu
          open={menuOpen}
          // @ts-ignore
          ref={innerRef}
        >
          {/* <NavigationItem href="/projects">Projects</NavigationItem> */}
          <NavigationItem external href="https://blog.truyol.dev">
            Blog
          </NavigationItem>
          <NavigationItem external href={`mailto:${personalInfo.email}`}>
            Contact me
          </NavigationItem>
        </NavigationMenu>
      </div>
    </NavigationBarContainer>
  );
};

export default NavigationBar;
