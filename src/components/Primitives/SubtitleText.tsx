import styled from "styled-components";
import { FontComponentProps } from "./FontComponentProps";

const SubtitleText = styled.span.attrs({
  className: "subtitle-text",
})<FontComponentProps>`
  font-weight: ${(props) =>
    props.bold ? "bold" : props.medium ? "600" : "normal"};
  font-size: 1rem;
  line-height: 1.375rem;
  letter-spacing: 0.009rem;
  display: inline-block;
`;

export default SubtitleText;
