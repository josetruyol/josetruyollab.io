import styled from "styled-components";

type ButtonLinkProps = {
  large?: boolean;
  small?: boolean;
  link?: boolean;
};

const ButtonText = styled.span.attrs({
  className: "button-link-text",
})<ButtonLinkProps>`
  font-weight: ${(props) => (props.small !== true ? "normal" : "600")};
  font-size: ${(props) =>
    props.link
      ? props.large
        ? "1.5rem"
        : props.small
        ? "0.875rem"
        : "1rem"
      : "1rem"};
  line-height: 1em;
  letter-spacing: ${(props) =>
    props.link
      ? props.large
        ? "0.011rem"
        : props.small
        ? "0.006rem"
        : "0.016rem"
      : "0.047rem"};
  text-decoration-line: ${(props) =>
    props.link && (props.large || !props.small) ? "underline" : ""};
  text-transform: ${(props) => (props.link ? "" : "capitalize")};
  display: inline-block;
`;

export default ButtonText;
