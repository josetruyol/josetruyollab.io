import styled from "styled-components";
import { FontComponentProps } from "./FontComponentProps";

const H6 = styled.h6<FontComponentProps>`
  font-weight: ${(props) => (props.bold ? "bold" : "normal")};
  font-size: 1.25rem;
  line-height: 1.875rem;
  letter-spacing: 0.009rem;
`;

export default H6;
