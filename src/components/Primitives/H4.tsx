import styled from "styled-components";
import { FontComponentProps } from "./FontComponentProps";

const H4 = styled.h4<FontComponentProps>`
  font-weight: ${(props) => (props.bold ? "bold" : "600")};
  font-size: 2.125rem;
  line-height: 2.563rem;
  letter-spacing: 0.016rem;
`;

export default H4;
