import styled from "styled-components";
import { FontComponentProps } from "./FontComponentProps";

const H3 = styled.h3<FontComponentProps>`
  font-weight: ${(props) => (props.bold ? "bold" : "600")};
  font-size: 3rem;
  line-height: 3.688rem;
`;

export default H3;
