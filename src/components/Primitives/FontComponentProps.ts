export interface FontComponentProps {
  bold?: boolean;
  medium?: boolean;
}
