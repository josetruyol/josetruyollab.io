import styled from "styled-components";
import { FontComponentProps } from "./FontComponentProps";

const H5 = styled.h5<FontComponentProps>`
  font-weight: ${(props) => (props.bold ? "bold" : "normal")};
  font-size: 1.5rem;
  line-height: 132%;
`;

export default H5;
