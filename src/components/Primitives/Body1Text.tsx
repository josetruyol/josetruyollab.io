import styled from "styled-components";
import { FontComponentProps } from "./FontComponentProps";

const Body1Text = styled.span.attrs({
  className: "body1-text",
})<FontComponentProps>`
  font-weight: ${(props) =>
    props.bold ? "bold" : props.medium ? "600" : "normal"};
  font-size: 1rem;
  line-height: 1.625rem;
  letter-spacing: 0.009rem;
  display: inline-block;
`;

export default Body1Text;
