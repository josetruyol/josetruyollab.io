import styled from "styled-components";

interface CircularIconProps {
  size?: string | number;
  variant?: "primary" | "secondary";
}

const CircularIcon = styled.div<CircularIconProps>`
  display: inline-block;
  border-radius: 100%;
  width: ${(props) => props.size || 40}px;
  height: ${(props) => props.size || 40}px;
  background-color: ${(props) =>
    props.variant === "secondary"
      ? props.theme.colors.brand.secondary.base
      : props.theme.colors.brand.primary.base};
  padding: 0.625rem;
  color: ${(props) =>
    props.variant === "secondary"
      ? props.theme.colors.brand.primary.base
      : "white"};
`;

export default CircularIcon;
