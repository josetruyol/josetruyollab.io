import React, { ReactElement, ReactNode } from "react";
import styled from "styled-components";

const CenteredGridContainer = styled.div<{ size: number }>`
  display: grid;
  gap: 4rem;
  justify-items: center;
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    grid-template-columns: repeat(${(props) => props.size}, 1fr);
    & > * {
      &.align-self-start {
        align-self: start;
      }
      &.align-self-center {
        align-self: center;
      }

      &.align-self-end {
        align-self: end;
      }
      &.justify-self-start {
        justify-self: start;
      }
      &.justify-self-center {
        justify-self: center;
      }
      &.justify-self-end {
        justify-self: end;
      }
    }
  }
`;

interface CenteredGridProps {
  columns: number;
  className?: string;
  children: (ReactElement | ReactNode)[];
}

const CenteredGrid = ({
  columns,
  children,
  className,
}: React.PropsWithChildren<CenteredGridProps>) => {
  if (children === undefined || children.length === 0) return <div></div>;
  const centerX = (columns - 1) / 2;
  const centerY = (children.length / columns - 1) / 2;
  return (
    <CenteredGridContainer size={columns} className={className}>
      {children.map((x, idx) => {
        const currentRow = Math.floor(idx / columns);
        const columnPosition = idx % columns;
        const justify =
          columnPosition < centerX
            ? "justify-self-end"
            : columnPosition === centerX
            ? "justify-self-center"
            : "justify-self-start";
        const align =
          currentRow < centerY
            ? "align-self-end"
            : columnPosition === centerX
            ? "align-self-center"
            : "align-self-start";
        return React.cloneElement(x as ReactElement, {
          className: `${justify} ${align}`,
          key: idx,
        });
      })}
    </CenteredGridContainer>
  );
};
export default CenteredGrid;
