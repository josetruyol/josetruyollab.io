import styled from "styled-components";

const Layout = styled.main`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin: 0 auto;
  align-items: center;
  max-width: 90rem;
  height: 100%;
  position: relative;
  z-index: 1;
  overflow-x: hidden;
`;

export default Layout;
