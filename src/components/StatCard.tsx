import styled from "styled-components";
import H4 from "./Primitives/H4";
import H6 from "./Primitives/H6";
import SubtitleText from "./Primitives/SubtitleText";

type StatCardProps = {
  big?: boolean;
  value: string;
  title: string;
  type?: "right" | "left";
};
type StatCardContainerProps = Pick<StatCardProps, "big" | "type">;

const StatCardContainer = styled.div<StatCardContainerProps>`
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 16px;
  justify-content: center;
  padding: 1rem;
  background-color: white;
  box-shadow: ${(props) => props.theme.shadows.dp04};
  width: ${(props) =>
    props.big ? "clamp(200px, 25vw, 360px)" : "clamp(200px, 25vw, 260px)"};

  height: ${(props) =>
    props.big ? "clamp(200px, 25vw, 360px)" : "clamp(200px, 25vw, 260px)"};
  /* &::after {
    content: "";
    display: block;
    padding-bottom: 50%;
  } */
  text-align: center;
  h4 {
    color: ${(props) => props.theme.colors.brand.primary.base};
    text-transform: uppercase;
  }
  .subtitle-text {
    color: ${(props) => props.theme.colors.layout.darken0};
  }
  .split-line {
    width: 148px;
    height: 1px;
    margin: 0 auto;
    background-color: ${(props) => props.theme.colors.brand.primary.base};
  }
  .circle {
    position: absolute;
    top: calc(50% - 3.5rem / 2);
    ${(props) => props.type ?? "left"}: calc(-3.5rem / 2);
    background-color: ${(props) => props.theme.colors.brand.secondary.base};
    width: 3.5rem;
    height: 3.5rem;
    border-radius: 100%;
  }
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    /* ${(props) => (props.big ? "height: 22.75rem;" : null)}; */
  }
`;

const StatCard = ({ title, value, ...props }: StatCardProps) => {
  const containerProps: StatCardContainerProps = props;
  return (
    <StatCardContainer {...containerProps}>
      <H4 bold>{value}</H4>
      <div className="split-line"></div>
      <SubtitleText>{title}</SubtitleText>
      <div className="circle"></div>
    </StatCardContainer>
  );
};
export default StatCard;
