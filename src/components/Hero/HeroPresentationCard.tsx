import H4 from "components/Primitives/H4";
import SubtitleText from "components/Primitives/SubtitleText";
import Image from "next/image";
import styled from "styled-components";
import heroImage from "../../assets/img/hero_mobile.svg";
import heroImageDesktop from "../../assets/img/hero_desktop.svg";
import personalInfo from "data/personalInfo";
import workExperience from "data/workExperience";

const HeroPresentationCardContainer = styled.article`
  background-color: white;
  flex-grow: 1;
  .text-container {
    padding: 24px;
    text-align: center;
  }
  h1 {
    font-size: 96px;
    font-weight: 700;
    line-height: 125px;
    letter-spacing: -1.5px;
    margin: 0px;
  }
  h4 {
    color: ${(props) => props.theme.colors.brand.primary.darken2};
    font-size: clamp(2.125rem, 5vw, 6rem);
    line-height: clamp(2.563rem, 5vw, 130%);
    white-space: nowrap;
  }
  .subtitle-text {
    padding: 16px;
    display: block;
    color: ${(props) => props.theme.colors.layout.darken0};
  }
  .hero-image__wrapper {
    width: 100%;
    max-width: 300px;
    margin: 0 auto;
  }
  .mobile {
    display: block !important;
    height: 100%;
  }
  .desktop {
    display: none !important;
  }
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    display: flex;
    min-height: 570px;
    .text-container {
      padding: 48px;
      flex: 1;
      text-align: left;
    }
    .hero-image__wrapper {
      min-width: clamp(300px, 50%, 700px);
      display: flex;
      .desktop {
        display: flex !important;
        width: 100%;
      }
      .mobile {
        display: none !important;
      }
      .desktop,
      .mobile {
        & > div {
          flex: 1;
          width: 100%;
          margin-top: -7rem !important;
        }
      }
    }
  }
`;

const HeroPresentationCard = ({ className }) => {
  return (
    <HeroPresentationCardContainer className={className}>
      <div className="text-container">
        <H4>{personalInfo.name}</H4>
        <SubtitleText>
          {workExperience[0].position} at {workExperience[0].company}
        </SubtitleText>
      </div>
      <div className="hero-image__wrapper">
        <div className="mobile">
          <Image
            src={heroImage}
            layout="responsive"
            aria-hidden="true"
            alt="Hero"
          />
        </div>
        <div className="desktop">
          <Image
            src={heroImageDesktop}
            layout="responsive"
            aria-hidden="true"
            alt="Hero"
          />
        </div>
      </div>
    </HeroPresentationCardContainer>
  );
};
export default HeroPresentationCard;
