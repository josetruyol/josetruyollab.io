import Body1Text from "components/Primitives/Body1Text";
import ButtonText from "components/Primitives/ButtonLinkText";
import H4 from "components/Primitives/H4";
import personalInfo from "data/personalInfo";
import styled from "styled-components";
import HeroContent from "./HeroContent";

const HeroContainer = styled.section`
  .hero-wrapper {
    width: 100%;
    padding: 0 24px;
    position: relative;
    display: flex;
    flex-direction: column;
    margin-top: 10rem;
    .hero__top-links {
      display: block;
      padding: 8px;
      color: ${(props) => props.theme.colors.brand.primary.base};
      @media only screen and (min-width: ${(props) =>
          props.theme.breakpoints.sm}) {
        padding: 16px 16px 16px 120px;
      }
    }
    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      display: grid;
      padding-left: 80px;
      padding-right: 0px;
    }
  }
  .who-am-i {
    position: relative;
    top: -5rem;
    background-color: ${(props) => props.theme.colors.layout.darken1};
    color: white;
    padding: 1.5rem;
    margin: 0 1.5rem;
    min-height: 485px;
    h4 {
      text-align: center;
    }
    .body1-text {
      margin-top: 0.5rem;
      padding: 1rem;
    }
    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      left: 35%;
      margin: 0;
      top: -11.25rem;
      padding: 120px clamp(0px, 14%, 190px) 120px clamp(0px, 5%, 110px);
      width: 65%;
      min-height: 600px;
    }
  }
`;
const Hero = () => {
  return (
    <HeroContainer>
      <div className="hero-wrapper">
        <div className="hero__top-links">
          <a
            href="https://gitlab.com/josetruyol/josetruyol.gitlab.io"
            target="_blank"
            rel="noreferrer"
          >
            <ButtonText link>GitLab Repository</ButtonText>
          </a>
        </div>
        <HeroContent></HeroContent>
      </div>

      <article className="who-am-i">
        <H4>Who am I?</H4>
        {personalInfo.whoAmI.split("\n").map((x, idx) => (
          <Body1Text key={idx}>{x}</Body1Text>
        ))}
        <Body1Text></Body1Text>
      </article>
    </HeroContainer>
  );
};

export default Hero;
