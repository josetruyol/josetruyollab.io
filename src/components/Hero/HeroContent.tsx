import styled from "styled-components";
import HeroPresentationCard from "./HeroPresentationCard";
import SocialNetworks from "../SocialNetworks";

const HeroContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  @media only screen and (min-width: ${(props) => props.theme.breakpoints.sm}) {
    /* height: 500px; */
    flex-direction: row;
    .header__presentation_card {
      flex: 1;
    }
  }
`;

const HeroContent = () => {
  return (
    <HeroContentContainer>
      <SocialNetworks variant="secondary" smColum></SocialNetworks>
      <HeroPresentationCard className="header__presentation_card"></HeroPresentationCard>
    </HeroContentContainer>
  );
};

export default HeroContent;
