import { useCallback, useEffect, useRef } from "react";

// @ts-ignore
const useOuterClick = (innerRef, callback) => {
  const callbackRef = useRef();
  useEffect(() => {
    callbackRef.current = callback;
  });
  const handleClick = useCallback(
    (e) => {
      if (
        innerRef?.current &&
        callbackRef.current &&
        !innerRef.current.contains(e.target)
      ) {
        // @ts-ignore
        callbackRef.current(e);
      }
    },
    [innerRef, callbackRef]
  );
  useEffect(() => {
    document.getElementById("root")?.removeEventListener("click", handleClick);
    document.getElementById("root")?.addEventListener("click", handleClick);
  }, [innerRef, callback, handleClick]);
};

export default useOuterClick;
