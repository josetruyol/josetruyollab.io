import "styles/globals.css";
import Theme from "components/Theme";
import type { AppProps } from "next/app";
import Script from "next/script";
import GlobalStyles from "components/GlobalStyles";
import Layout from "components/Layout";
import NavigationBar from "components/NavigationBar";
import Footer from "components/Footer";
import * as ga from "utils/ga";
import { useRouter } from "next/router";
import { useEffect } from "react";

function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  useEffect(() => {
    const handleRouteChange = (url: URL) => ga.gPageView(url);
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);
  return (
    <Theme>
      <>
        <GlobalStyles />
        <Script
          src={`https://www.googletagmanager.com/gtag/js?id=${ga.GA_TRACKING_ID}`}
          strategy="afterInteractive"
        />
        <Script id="google-analytics" strategy="afterInteractive">
          {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${ga.GA_TRACKING_ID}');
          `}
        </Script>
        <NavigationBar></NavigationBar>
        <Layout>
          <Component {...pageProps} />
          <Footer />
        </Layout>
      </>
    </Theme>
  );
}
export default App;
