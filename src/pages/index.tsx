import Carousel from "components/Carousel";
import CenteredGrid from "components/CenteredGrid";
import CertificationCard from "components/CertificationCard";
import Hero from "components/Hero";
import H4 from "components/Primitives/H4";
import StatCard from "components/StatCard";
import type { GetStaticProps, NextPage } from "next";
import Head from "next/head";
import { format as dateFormat } from "date-fns";
import styled from "styled-components";
import WorkCard from "components/WorkCard";
import personalInfo from "data/personalInfo";
import certifications from "data/certifications";
import workExperience from "data/workExperience";

const getStaticProps: GetStaticProps = async (context) => {
  return {
    props: {
      now: dateFormat(new Date(), "MM/yyyy"),
    },
  };
};

const PageContainer = styled.div`
  width: 100%;

  .primary-section {
    padding: 1.5rem;
    background-color: ${(props) => props.theme.colors.layout.darken1};

    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      padding: 2rem;
      .title {
        padding: 5rem 3rem;
      }
    }
  }
  .stats-grid {
    margin-top: -8rem;
    margin-bottom: 10rem;
    @media only screen and (min-width: ${(props) =>
        props.theme.breakpoints.sm}) {
      margin-top: -22rem;
    }
  }
  .projects-section {
    text-align: center;
    margin: 5rem 0;
    & > h4 {
      color: ${(props) => props.theme.colors.layout.darken0};
    }
  }
`;

const Home: NextPage = ({ now }: any) => {
  return (
    <>
      <Head>
        <title>Jose Truyol | Home</title>
      </Head>
      <PageContainer>
        <section>
          <Hero />
          <CenteredGrid columns={2} className="stats-grid">
            {personalInfo.stats.map((x, idx, arr) => (
              <StatCard
                key={idx}
                big={idx === 0 || idx === arr.length - 1}
                type={idx % 2 === 0 ? "left" : "right"}
                title={x.title}
                value={x.value}
              />
            ))}
            {/*<StatCard big title="title1" value="as"></StatCard>
            <StatCard type="right" title="title1" value="as"></StatCard>
            <StatCard title="title1" value="as"></StatCard>
            <StatCard big type="right" title="title1" value="as"></StatCard> */}
          </CenteredGrid>
        </section>
        <section className="primary-section">
          <H4 bold className="title">
            Work Experience
          </H4>

          <Carousel>
            {workExperience.map((x, idx) => (
              <WorkCard
                key={idx}
                title={x.position}
                company={x.company}
                from={x.from}
                to={x.to ?? "present"}
                duration={x.duration}
                location={x.location}
                description={x.description}
              />
            ))}
          </Carousel>
        </section>
        <section className="projects-section">
          <H4 bold>Projects</H4>
          <br />
          <br />
          <H4>Coming Soon</H4>
        </section>
        <section className="primary-section">
          <H4 bold className="title">
            Certifications
          </H4>
          <Carousel>
            {certifications.map((x, idx) => (
              <CertificationCard
                key={idx}
                issuer={x.issuer}
                title={x.title}
                from={x.from}
                to={x.to}
              />
            ))}
          </Carousel>
        </section>
        {/* <section>Skills</section> */}
      </PageContainer>
    </>
  );
};

export default Home;
export { getStaticProps };
