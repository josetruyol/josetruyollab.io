import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    breakpoints: {
      sm: string;
    };
    colors: {
      brand: {
        primary: {
          base: string;
          darken0: string;
          darken1: string;
          darken2: string;
          lighten0: string;
        };
        secondary: {
          base: string;
          darken0: string;
          darken1: string;
          darken2: string;
        };
      };
      layout: {
        darken0: string;
        darken1: string;
        darken2: string;
        darken3: string;
        darken4: string;
        lighten0: string;
        lighten1: string;
        lighten2: string;
        lighten3: string;
        lighten4: string;
        overlay1: string;
        overlay2: string;
        overlay3: string;
      };
    };
    shadows: {
      dp01: string;
      dp02: string;
      dp03: string;
      dp04: string;
      dp05: string;
      dp06: string;
    };
    backgroundColor: string;
    menu: {
      activeColor: string;
      mobile: {
        background: string;
      };
      desktop: {
        textColor: string;
        activeTextColor: string;
      };
    };
  }
}
