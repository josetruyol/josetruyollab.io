import { differenceInBusinessDays, differenceInCalendarYears } from "date-fns";

const personalInfo = {
  name: "José Truyol",
  position: "Senior Full Stack Developer Engineer",
  email: "jose@truyol.dev",
  socialNetworks: {
    linkedInd: "https://www.linkedin.com/in/josetruyol/",
    twitter: "https://twitter.com/xolott",
    instagram: "https://www.instagram.com/josetruyol/",
    gitLab: "https://gitlab.com/josetruyol/",
    gitHub: "https://github.com/xolott",
  },
  whoAmI: `I'm a senior software engineer passionate about employing the newest technologies to deliver quality products and services in this digital world to facilitate people's lives.
During my college time, after my first C++programming class, I started learning python on a self-taught basis, and there is where my love for this profession began.
Using the best of my abilities and goodwill to always make the best and greatest impact in any company I worked (and will work) for.`,
  footerNote:
    "I'm a very easy-to-talk guy with strong communication and technical skills, and willing to apply all my knowledge and abilities to create and maintain the next big product in my next position.",
  stats: [
    {
      title: "Projects involved",
      value: "+15",
    },
    {
      title: "Years of experience",
      value: "+" + differenceInCalendarYears(new Date(), new Date(2014, 10)),
    },
    {
      title: "BBQs with family and friends",
      value: "+50",
    },
    {
      title: "Lines of code written",
      value:
        "+" +
        (differenceInBusinessDays(new Date(), new Date(2014, 1, 1)) * 100) /
          1000 +
        "K",
    },
  ],
};

export default personalInfo;
