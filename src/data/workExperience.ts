import { differenceInCalendarMonths } from "date-fns";

const calculateDuration = (start: Date, end?: Date) => {
  const totalMonths = differenceInCalendarMonths(end ?? new Date(), start);
  const years = Math.floor(totalMonths / 12);
  const months = totalMonths % 12;
  return `${years > 0 ? years + " years and " : ""}${months} months`;
};

const workExperience = [
  {
    position: "Senior Software Engineer",
    company: "Indimin",
    from: "January 2020",
    to: null,
    duration: calculateDuration(new Date(2019, 12)),
    location: "Santiago, Chile",
    description:
      "Indimin is the first and most important company in LATAM to bring AI to the mining sector, to connect the front line workers with the data, increase the productivity and customer's income.",
  },
  {
    position: "Software Engineer - Freelancer",
    company: "Indimin",
    from: "July 2019",
    to: "January 2020",
    duration: calculateDuration(new Date(2019, 7), new Date(2020, 2)),
    location: "Santiago, Chile",
    description:
      "Indimin is the first and most important company in LATAM to bring AI to the mining sector, to connect the front line workers with the data, increase the productivity and customer's income.",
  },
  {
    position: "Senior Development Engineer",
    company: "Namku",
    from: "April 2017",
    to: "December 2019",
    duration: calculateDuration(new Date(2017, 4), new Date(2020, 1)),
    location: "Santiago, Chile",
    description:
      "Namku was a Startup whose sole purpose was to bring AI to the security market delivering quality products based on Face Recognition, and to the lawyers developing text recognition and categorization of legal documents.",
  },
  {
    position: "Software and Hardware Engineer",
    company: "Axys S.A.",
    from: "February 2016",
    to: "March 2017",
    duration: calculateDuration(new Date(2016, 2), new Date(2017, 4)),
    location: "Santiago, Chile",
    description:
      "Axys is the leader in solutions development for the mining sector in Chile, delivering custom hardware and software impacting communications and human resources management.",
  },
  {
    position: "Hardware Development Engineer",
    company: "The Factory HKA",
    from: "October 2014",
    to: "September 2015",
    duration: calculateDuration(new Date(2014, 10), new Date(2015, 10)),
    location: "Caracas, Venezuela",
    description:
      "The Factory HKA is the leader in the fiscal printer market in Venezuela, continuously investing in R&D to improve the design of their products, the user experience, and the security of sensitive data.",
  },
];

export default workExperience;
