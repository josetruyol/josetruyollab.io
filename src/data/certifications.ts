const certifications = [
  {
    title: "React",
    issuer: "Udemy",
    from: "10/2021",
    to: null,
  },
  {
    title: "GitLab Certified Associate",
    issuer: "GitLab",
    from: "05/2021",
    to: null,
  },
  {
    title: "M001: MongoDB Basics",
    issuer: "MongoDB",
    from: "05/2019",
    to: null,
  },
  {
    title: "M121: MongoDB Aggregation Framework",
    issuer: "MongoDB",
    from: "05/2019",
    to: null,
  },
  {
    title: "CS188.1X Artificial Intelligence",
    issuer: "edX",
    from: "05/2013",
    to: null,
  },
  {
    title: "6.00X Introduction to Computer Science and Programming",
    issuer: "edX",
    from: "01/2013",
    to: null,
  },
  {
    title: "Japanese Language Proficiency Test",
    issuer: "The Japan Foundation",
    from: "12/2010",
    to: null,
  },
];

export default certifications;
